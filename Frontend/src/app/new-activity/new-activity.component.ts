import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../models/Activity/Activity';
import { SetActivity } from '../models/Activity/setActivity';

import { AdvertSet } from '../models/Adverts/AdvertSet';
import { ActivityService } from '../_services/Activity/activity.service';
import { AdvertsService } from '../_services/Adverts/adverts.service';
import { SnackBarService } from '../_services/SnackBar/snack-bar.service';
@Component({
    selector: 'app-new-activity',
    templateUrl: './new-activity.component.html',
    styleUrls: ['./new-activity.component.scss'],
})
export class NewActivityComponent implements OnInit {
    NewActivityForm: FormGroup;
    constructor(
        private route: ActivatedRoute,
        private fB: FormBuilder,
        private actService: ActivityService,
        private snackbar: SnackBarService,
        private rt: Router
    ) {}
    id = this.route.snapshot.paramMap.get('id');
    usrId = localStorage.getItem('usr_id');
    ngOnInit(): void {
        this.initForm();
    }

    initForm() {
        this.NewActivityForm = this.fB.group({
            name: ['', [Validators.required]],
            adress: ['', [Validators.required]],
            city: ['', [Validators.required]],
            act_type: ['', [Validators.required]],
            act_price: [
                '',
                [Validators.required, Validators.pattern(/^[0-9]\d*$/)],
            ],

            act_postal: [
                '',
                [Validators.required, Validators.pattern(/[0-9]{5}/)],
            ],
            describe: ['', [Validators.required]],
        });
    }

    onSubmit(NewActivityFormData) {
        const activity: SetActivity = {
            act_name: NewActivityFormData.name,
            act_adv_id: Number(this.id),
            act_adress: NewActivityFormData.adress,
            act_city: NewActivityFormData.adress,
            act_describe: NewActivityFormData.describe,
            act_postal: NewActivityFormData.act_postal,
            act_price: NewActivityFormData.act_price,
            act_type: NewActivityFormData.act_type,
            act_usr_id: Number(this.usrId),
        };
        console.log(activity);
        this.snackbar.openSnackBar('Sauvegarde en cours ...', 'ok', 1500);

        this.actService.createActivity(activity).then((result) => {
            if (result.error !== undefined && result.error.length > 0) {
                this.snackbar.openSnackBar(result.error, 'ok', 1500);
            } else {
                this.snackbar.openSnackBar('Activité sauvegardé', 'ok', 1500);
                this.rt.navigate(['/search']);
            }
        });
    }
}
