import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Activity } from 'src/app/models/Activity/Activity';
import { SetActivity } from 'src/app/models/Activity/setActivity';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ActivityService {
    constructor(public rt: Router) {}

    async getActivityById(id: number) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                adv_id: id,
            }),
        };

        const response = await fetch(
            environment.API_URL + '/getActivityById',
            requestOptions
        );
        const data = await response.json();
        return data;
    }

    async createActivity(activity: SetActivity) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                act_name: activity.act_name,
                act_adv_id: activity.act_adv_id,
                act_usr_id: activity.act_usr_id,
                act_adress: activity.act_adress,
                act_city: activity.act_city,
                act_postal: activity.act_postal,
                act_describe: activity.act_describe,
                act_price: activity.act_price,
                act_type: activity.act_type,
            }),
        };

        console.log(requestOptions);
        const response = await fetch(
            environment.API_URL + '/createActivity',
            requestOptions
        );
        const result = await response.json();
        return result;
    }
}
