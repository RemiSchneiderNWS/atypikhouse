import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Advert } from '../models/Adverts/Advert';
import { Activity } from '../models/Activity/Activity';
import { AdvertsService } from '../_services/Adverts/adverts.service';
import { StatusUser } from '../_services/User/statusUser';
import { ActivityService } from '../_services/Activity/activity.service';
import { Commentary } from '../models/Commentary/Commentary';
import { CommentaryService } from '../_services/Commentary/commentary.service';
import { ReserveService } from '../_services/Reserve/reserve.service';
import { DatesReserve } from '../models/Reserve/datesReserve';
import { SnackBarService } from '../_services/SnackBar/snack-bar.service';
import { Reserve } from '../models/Reserve/Reserve';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidReserve } from '../models/Reserve/validReserve';

@Component({
    selector: 'app-update-advert-board',
    templateUrl: './update-advert-board.component.html',
    styleUrls: ['./update-advert-board.component.scss'],
})
export class UpdateAdvertBoardComponent implements OnInit {
    id: string;
    advert: Advert;
    datesReserve: FormGroup;
    datesErrors: boolean = false;
    listActivity: Activity[];
    datesReserved: DatesReserve[];
    listCommentary: Commentary[];
    mailVisible: boolean = false;
    phoneVisible: boolean = false;
    notOwner: boolean = true;
    usr_id: number;
    usr_firstName: string;
    usr_lastName: string;

    userCanComment: boolean = false;

    currentYear = new Date().getFullYear();
    minDate = new Date(2020, 1, 1);
    minDatePicker = new Date();
    maxDate = new Date(this.currentYear + 1, 11, 31);

    constructor(
        private snackbar: SnackBarService,
        private route: ActivatedRoute,
        private rt: Router,
        private adv: AdvertsService,
        private act: ActivityService,
        private com: CommentaryService,
        private fB: FormBuilder,
        public statusUser: StatusUser,
        private rs: ReserveService
    ) {
        this.datesReserve = this.fB.group({
            start: [, [Validators.required]],
            end: [, [Validators.required]],
        });
    }

    ngOnInit(): void {
        console.log('1');
        this.id = this.route.snapshot.paramMap.get('id');
        this.usr_id = Number(localStorage.getItem('usr_id'));
        this.usr_firstName = localStorage.getItem('usr_firstName');
        this.usr_lastName = localStorage.getItem('usr_lastName');
        this.adv.getAdverById(Number(this.id)).then((data) => {
            this.advert = data.selectedAdvert;

            let advUsrId: number = Number(this.advert[0].adv_usr_id);

            let usrId: number = Number(localStorage.getItem('usr_id'));

            if (advUsrId === usrId) {
                this.notOwner = false;
            } else {
                this.rt.navigate(['/home']);
            }
        });
        this.act.getActivityById(Number(this.id)).then((data) => {
            this.listActivity = data.selectedActivity;
        });
        console.log('2');

        this.com.getCommentaryByAdvert(Number(this.id)).then((data) => {
            this.listCommentary = data.selectedCommentary;
        });

        this.rs.getDatebyAdvRes(Number(this.id)).then((data) => {
            this.datesReserved = data.result;
            console.log(this.datesReserved);
        });
    }

    myFilter = (d: Date | null): boolean => {
        console.log('4');
        if (this.datesReserved != null && this.datesReserved.length > 0) {
            for (let i = 0; i < this.datesReserved.length; i++) {
                if (
                    d >=
                        this.getFormattedDate(
                            this.datesReserved[i].res_date_start
                        ) &&
                    d <=
                        this.getFormattedDate(
                            this.datesReserved[i].res_date_end
                        )
                ) {
                    return false;
                }
            }
        }
        return true;
    };
    onSubmit(datesReserve) {
        let dateStartToCompare: Date;
        let dateEndToCompare: Date;

        for (let i = 0; i < this.datesReserved.length; i++) {
            dateStartToCompare = new Date(this.datesReserved[i].res_date_start);
            dateEndToCompare = new Date(this.datesReserved[i].res_date_end);
            if (
                datesReserve.start < dateStartToCompare &&
                datesReserve.end >= dateEndToCompare
            ) {
                this.datesErrors = true;
                this.snackbar.openSnackBar(
                    'Vous ne pouvez pas sélectionner une période réservée',
                    'ok',
                    1500
                );
            }
        }
        if (!this.datesErrors) {
            let dateStart: Date = new Date(
                this.getFormattedDateForApi(datesReserve.start)
            );
            let dateEnd: Date = new Date(
                this.getFormattedDateForApi(datesReserve.end)
            );
            let newReserve: Reserve = {
                res_adv_id: Number(this.advert[0].adv_id),
                res_adv_price: this.advert[0].adv_price,
                res_adv_tenants: 0,
                res_adv_name: this.advert[0].adv_name,
                res_usr_mail: localStorage.getItem('usr_mail'),
                res_usr_phone: localStorage.getItem('usr_phone'),
                res_usr_id: Number(localStorage.getItem('usr_id')),
                res_date_start: dateStart,
                res_date_end: dateEnd,
            };
            this.rs.createReserve(newReserve).then(() => {
                let reserveToApprove: ValidReserve = {
                    res_adv_id: newReserve.res_adv_id,
                    res_payment: true,
                    res_usr_id: Number(localStorage.getItem('usr_id')),
                    res_date_start: newReserve.res_date_start,
                    res_date_end: newReserve.res_date_end,
                };

                this.rs.validReserve(reserveToApprove).then(() => {
                    this.snackbar.openSnackBar('période bloqué', 'ok', 1500);
                });
            });
        }
    }

    getFormattedDate(dateReceive: Date) {
        let date = new Date(dateReceive);
        let year = date.getFullYear();
        let month = (1 + date.getMonth()).toString().padStart(2, '0');
        let day = date.getDate().toString().padStart(2, '0');

        return new Date(month + '-' + day + '-' + year);
    }

    getFormattedDateForApi(dateReceive: Date) {
        let date = new Date(dateReceive);
        let year = date.getFullYear();
        let month = (1 + date.getMonth()).toString().padStart(2, '0');
        let day = date.getDate().toString().padStart(2, '0');

        return new Date(year + '-' + month + '-' + day);
    }

    back() {
        this.rt.navigate(['/search']);
    }

    setMailVisible() {
        this.mailVisible = true;
    }

    setPhoneVisible() {
        this.phoneVisible = true;
    }
}
